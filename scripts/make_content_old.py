import os
from datetime import datetime
import requests
from bs4 import BeautifulSoup
import requests

#####################
### useful functions
#####################

### intro page content
def IntroContent():
    ### line by line markdown code
    introArr=["## Welcome ITk Report Repository",
          "This is intended as a repository for [datapane](https://datapane.com) reports of ITk Production Database data (usually using [itk-reports](https://gitlab.cern.ch/wraight/itk-reports).)",
          "## Code repository",
          "Code to generate these pages lives [here](https://gitlab.cern.ch/wraight/itk-reports-repository)",
          "## Report Generation",
          "Some documentation on generating reports with _ITk-reports_ can be found [here]().",
          "### Some Credits",
          "Built using [squidfunk](https://squidfunk.github.io/mkdocs-material/)",
          " - additional inspitation from [itk-docs](https://itk.docs.cern.ch)",
          "!!! notice",
          f"\tPage generated: {datetime.now().date()} ({datetime.now().time()})",
          "\tFor updates/fixes contact: wraightATcern.ch"
        ]
    ### convert to string
    introStr=""
    for ia in introArr:
        introStr+=f"{ia}\n\n"
    
    return introStr


### quick key of acronymns
def QuickKey():
    retStr=[]
    retStr.append("!!! acronyms ")
    retStr.append(f"\tCTDB &rarr; componentType Dashboard")
    retStr.append(f"\tTTDB &rarr; testType Dashboard")
    retStr.append(f"\tIIDB &rarr; institution inventory Dashboard")
    return retStr

### get report creation timestamp
def GetReportCreation(fileName):

    with open(fileName, 'rt') as html_file:
        for line in html_file:
            if 'Made on:' in line:
                return line[line.find('Made on:'):line.find(', by')]
    ### Made on: 2023-08-15 @ 11:58:50, by
    return "No creation found"

### get (html) files in directory
def GetFiles(inPath, ext="html"):

    if not os.path.isdir(inPath):
        print(f"Path ({inPath}) not found")
        return []

    retList=[]
    for f in sorted(os.listdir(inPath)):
        if ext not in f: continue
        print(f"\t\tadding {f}")
        retList.append({'path':f,'date':GetReportCreation(inPath+f)})
    
    return retList

def GetLinks(url, snip="href"):
    reqs = requests.get(url)
    soup = BeautifulSoup(reqs.text, 'html.parser')

    links = []
    for thing in soup.find_all('a'):
        link=thing.get(snip).replace('./','')
        # print(link)
        if "http://cern" in link or "https://cern" in link or"http://www.cern" in link  or "http://itssb" in link: # ignore standard cern stuff
            continue
        links.append(link)

    return links

def GetCreationData(url,startStr,endStr):

    ### get page
    page = requests.get(url)
    ### get subStr
    startInd=page.text.find(startStr)
    endInd=page.text[startInd::].find(endStr)
    # print(startInd,endInd)
    subStr=page.text[startInd:startInd+endInd]
    
    try:
        retStr=f"\n??? note \"Created {subStr.split(',')[0].split(':')[1].split('@')[0].strip()}\" \n"
        # retStr+="\n\n "
        for ss in subStr.split(',')[0:2]:
            retStr+=f"\n\t{ss.strip()}"
        retStr+="\n\n"

    except:
        print(f"- Issue reading: {subStr}")
        print(f"- url: {url}")

        retStr=f"\n??? note \"Issue reading{subStr}\" \n\n"

    return retStr


### make list of links per report type
def MakeLinksList(sourceName,repList):

    ### source
    retStr=f"Reports from: {sourceName}\n\n"

    ### if nothing found
    if len(repList)==0:
        retStr+="__no reports found__\n\n"
        return retStr

    ### no grouping for general reports
    if "general-reports" in sourceName:
        retStr+=f"### Reports \n\n"
        for rl in repList:
            retStr+=f" - [{rl.replace('.html','')}]({sourceName}/{rl})\r\n" 
            retStr+=GetCreationData(f"{sourceName}/{rl}","Made on","\\n\\n")
        return retStr

    ### split (non-general) reports into groups
    # define groups
    groupDict={'CTDB':{'name':"ComponentType Dashboard",'repList':[]},
              'TTDB':{'name':"TestType Dashboard",'repList':[]},
              'IIDB':{'name':"Institution Inventory Dashboard",'repList':[]},
              '_other_':{'name':"Other Dashboard",'repList':[]}
    }
    # group reports
    for rl in repList:
        found=False
        for k in groupDict.keys():
            if k in rl:
                groupDict[k]['repList'].append(rl)
                found=True
                break
        if not found:
            groupDict['_other_']['repList'].append(rl)
    
    ### publish groups
    for k in groupDict.keys():
        retStr+=f"### {groupDict[k]['name']}s ({len(groupDict[k]['repList'])})\n\n"
        for rl in groupDict[k]['repList']:
            retStr+=f" - [{rl.replace('.html','')}]({sourceName}/{rl})\r\n" 
            retStr+=GetCreationData(f"{sourceName}/{rl}","Made on","\\n\\n")

    return retStr

### write markdown page
def WritePage(wbs):
    
    with open(wbs['fileName'], "w") as content_file:
        ### header        
        if "code" in wbs.keys() and "name" in wbs.keys():
            content_file.write(f"# WBS {wbs['code']}: {wbs['name'].title()} \n\n")
        else:
            content_file.write(f"# Other reports \n\n")

        for d in QuickKey():
            content_file.write(f"{d}\n\n")

        # ## content
        # content_file.write(content)

#####################
### main function
#####################
def main():

    ### find insteresting directories
    print("Finding reports and write pages")
    for rt in ['strips','pixels','common_mechanics','common_electronincs','general','test']:
        url=f'https://wraight.web.cern.ch/{rt}-reports'
        print(f"\tworking on: {url}")
        linkList=GetLinks(url)
        print(f"\t- found: {len(linkList)} reports")
        listStr=MakeLinksList(url, linkList)
        WritePage(os.getcwd()+"/docs/"+rt+"-reports.md", listStr)
    
    ### find interesting directories
    print("Finding report pages")
    contentFiles=[]
    for doc in sorted(os.listdir(os.getcwd()+"/docs")):
        if os.path.isfile(os.getcwd()+"/docs/"+doc) and "reports.md" in doc:
            print(f"\tcollecting content in {doc}")
            contentFiles.append(os.getcwd()+"/docs/"+doc)
    print(f"\tfound {len(contentFiles)} reports pages")


    ### write index (with timestamp)
    print("Writing index")
    WritePage(os.getcwd()+"/docs/index.md",IntroContent())

    ### update yml
    with open("mkdocs.yml", "a") as mkdocs_file:
        for cf in contentFiles:
            newStr=f"    - {cf.split('/')[-1].split('-')[0].title()}: {cf.split('/')[-1]}\n"
            mkdocs_file.write(newStr)


### for commandline running
if __name__ == "__main__":
    main()
    print("All done.")
