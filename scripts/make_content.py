### imports
import json
import os
from bs4 import BeautifulSoup
import requests
from datetime import datetime

#####################
### useful functions
#####################

### Find reports

### Get report creation data
def GetReportCreationData(url,startStr,endStr):

    ### get page
    page = requests.get(url)
    ### get subStr
    startInd=page.text.find(startStr)
    endInd=page.text[startInd::].find(endStr)
    # print(startInd,endInd)
    subStr=page.text[startInd:startInd+endInd]
    
    try:
        retStr=f"\n??? note \"Created {subStr.split(',')[0].split(':')[1].split('@')[0].strip()}\" \n"
        # retStr+="\n\n "
        for ss in subStr.split(',')[0:2]:
            retStr+=f"\n\t{ss.strip()}"
        retStr+="\n\n"

    except:
        print(f"- Issue reading: {subStr}")
        print(f"- url: {url}")

        retStr=f"\n??? note \"Issue reading{subStr}\" \n\n"

    return retStr

def GetLinks(url, snip="href", creationNote=False):
    reqs = requests.get(url)
    soup = BeautifulSoup(reqs.text, 'html.parser')

    links = []
    for thing in soup.find_all('a'):
        name=thing.get(snip).replace('./','')
        # print(link)
        if "http://cern" in name or "https://cern" in name or"http://www.cern" in name  or "http://itssb" in name or "html" not in name: # ignore standard cern stuff
            continue
        prefix=url
        if prefix[-1]!="/":
            prefix+="/"
        print(f"- got link for: {name}")
        links.append({'link':prefix+name, 'name':name, 'note':None})
        if creationNote:
            print(" - ... and the creation date ...")
            links[-1]['note']=GetReportCreationData(prefix+name, "Made on","\\n\\n")
            print(" - done.")
    return links

### Write files

### quick key of acronymns
def QuickKey():

    retStr=[]
    retStr.append("!!! note \"acronyms\" ")
    retStr.append("\tCTDB :material-arrow-right: Component Type DashBoard")
    retStr.append("\tTTDB :material-arrow-right: Test Type DashBoard")
    retStr.append("\tIIDB :material-arrow-right: Institution Inventory DashBoard")
    retStr.append("\tPPDB :material-arrow-right: Part Populations DashBoard")

    return retStr

### make list of links per report type with sub-structure fro report types
def MakeLinksList(repList):

    print(repList)

    ### source (everything but the last bit)
    sourceName="/".join(repList[0]['link'].split('/')[0:-1])
    print(f"  - source: {sourceName}")
    retStr=f"Reports from: {sourceName}\n\n"

    ### if nothing found
    if len(repList)==0:
        print(" - no reports found")
        retStr+="__no reports found__\n\n"
        return retStr

    ### no grouping for general reports
    if "general-reports" in sourceName or "test-reports" in sourceName:
        print(f"  - {len(repList)} general-reports reports")
        retStr+=f"### Reports \n\n"
        for rep in repList:
            retStr+=f" - [{rep['name']}]({rep['link']})\r\n" 
            if rep['note']!=None:
                retStr+=f" {rep['note']}\r\n" 
            retStr+="\n"
            # retStr+=GetReportCreationData(rep['link'],"Made on","\\n\\n")
        return retStr

    ### split (non-general) reports into groups
    # define groups
    groupDict={'CTDB':{'name':"ComponentType Dashboard",'repList':[]},
              'TTDB':{'name':"TestType Dashboard",'repList':[]},
              'IIDB':{'name':"Institution Inventory Dashboard",'repList':[]},
              'PPDB':{'name':"Part Populations Dashboard",'repList':[]},
              '_other_':{'name':"Other Dashboard",'repList':[]}
    }
    # group reports
    for rep in repList:
        found=False
        for k in groupDict.keys():
            if k in rep['link']:
                groupDict[k]['repList'].append(rep)
                found=True
                break
        if not found:
            groupDict['_other_']['repList'].append(rep)
    
    ### publish groups
    for k in groupDict.keys():
        print(f"  - {len(groupDict[k]['repList'])} {k} reports")
        retStr+=f"### {groupDict[k]['name']}s ({len(groupDict[k]['repList'])})\n\n"
        for rep in groupDict[k]['repList']:
            retStr+=f" - [{rep['name']}]({rep['link']})\r\n" 
            if rep['note']!=None:
                retStr+=f" {rep['note']}\r\n" 
            retStr+="\n"
            # retStr+=GetReportCreationData(rep['link'],"Made on","\\n\\n")

    return retStr

### write markdown page
def WriteWBSPage(wbs):
    
    print(f"Writing: {wbs['fileName']}")
    with open(wbs['fileName'], "w") as content_file:

        ### header 
        print("- header...")
        # project code (by initials)
        pc = ''.join([x[0].upper() for x in wbs['project'].split(' ')])

        # WBS code and name
        if "code" in wbs.keys() and "name" in wbs.keys():
            content_file.write(f"# WBS {wbs['code']}: {wbs['name'].title()}  ({pc})\n\n")
        else:
            content_file.write(f"# Other reports \n\n")
        content_file.write("## Header Info. \n\n")
        # ACs
        content_file.write("### Responsible people \n" )
        if "ACs" in wbs.keys() and len(wbs['ACs'])>0:
            content_file.write("\n - "+"\n - ".join(sorted(wbs['ACs'])) )
        else:
            content_file.write("\n _no ACs defined_")
        content_file.write("\n\n" )
        # component types
        content_file.write("### Included component types \n" )
        if "CTs" in wbs.keys() and len(wbs['CTs'])>0:
            content_file.write("\n - "+"\n - ".join(sorted(wbs['CTs'])) )
        else:
            content_file.write("\n _no component types defined_")
        content_file.write("\n\n" )
        
        ### make content
        print("- reports...")
        content_file.write("## Reports \n\n")

        if len(wbs['reports'])>0:
            # acronym keys
            for d in QuickKey():
                content_file.write(f"{d}\n\n")
            # links - the main bit!
            print(f"- {len(wbs['reports'])} reports found")
            content_file.write(MakeLinksList(wbs['reports']))
        else:
            print("- no reports!")
            content_file.write("\n _no reports found_")
        content_file.write("\n\n" )


### intro page content
def IntroContent():
    ### line by line markdown code
    introArr=["## Welcome ITk Data Integrity Reports Repository",
          "This is intended as a repository for _Data Integrity_ dashboard reports from ITk Production Database (usually using [itk-reports](https://gitlab.cern.ch/wraight/itk-reports)).",
          "_Production Quality_ reporting is done elsewhere:",
          " - Pixels repository: https://gitlab.cern.ch/atlas-itk/pixel/module/module-qc-statistical-tools",
          " - Strips webApp: https://itkreporting-site-itkreporting.app.cern.ch/",
          "!!! notice",
          f"\tPage generated: {datetime.now().date()} ({datetime.now().time()})",
          "\tFor updates/fixes contact: wraightATcern.ch"
        ]
    ### convert to string
    introStr=""
    for ia in introArr:
        introStr+=f"{ia}\n\n"
    
    return introStr


### build WBS overview table
def BuildTableWBS(wbsList):
    #define string list
    tableArr=[]
    # table header
    tableArr.append("| code |  project | name |  ACs | #reports |")
    tableArr.append("| --- | --- | --- | --- | --- |")
    # table rows
    for wbs in wbsList:
        acStr=",".join(wbs['ACs'])
        tableArr.append(f"| {wbs['code']} | {wbs['project']} | {wbs['name']} | {acStr} | {len(wbs['reports'])} |")
    # return
    return tableArr


#####################
### main function
#####################
def main():

    ###################
    ### 1. find content: reports on EOS and compile to list
    ###################
    print("\n### Find reports")
    print("Finding reports and write pages")
    # reports in EOS in sub-directories
    url=f'https://wraight.web.cern.ch/'
    linkList=[]
    for proj in ['common_electronics','common_mechanics','pixels','strips','test','general']:
        proj_url=url+f"{proj}-reports"
        print(f"\tworking on: {proj} \n\t- {proj_url}")
        proj_links=GetLinks(proj_url, "href", True) # , "href", True) # get creation info. if true
        print(f"\t- found: {len(proj_links)} reports")
        linkList.extend(proj_links)
    # print(linkList)
    print(f"Final count of files: {len(linkList)}")

    ###################
    ### 2. structure content: read in WBS list to match PDB componentTypes to WBS codes
    ###################
    print("\n### Match reports to WBS")
    wbsList=[]
    with open('wbsList.json') as json_file:
        data = json.load(json_file)
        # print(data)
        # formatting - add CT  and report lists
        wbsList=data['wbsList']

    for wbs in wbsList:
        print(f"\n###########\n### {wbs['code']}\n###########")
        # add extra keys
        for k in ["CTs", "reports"]:
            if k not in wbs.keys():
                wbs[k]=[]
        ### make list of snippets for relevant reports
        # CTDB (will work also for PPDB)
        repList=["-"+ct+"." for ct in wbs['CTs']]
        # TTDB
        repList.extend(["-"+ct+"-" for ct in wbs['CTs']])
        print(f" - rep list ({len(repList)}): {repList}") #-P-MODULE.html
        ### loop codes-->projects
        if "2.1." in wbs['code']:
            wbs['project']="pixels"
            # assign reports
            wbs['reports']= [ rt for rt in linkList if "-P-" in rt['name'] and any(snip in rt['name'] for snip in repList) ] 
            print(f" - wbs size: {len(wbs['reports'])}")
            # filter now assigned reports
            linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbs['reports']], linkList))
        elif "2.2." in wbs['code']:
            wbs['project']="strips"
            # assign reports
            wbs['reports']= [ rt for rt in linkList if "-S-" in rt['name'] and any(snip in rt['name'] for snip in repList) ] 
            print(f" - wbs size: {len(wbs['reports'])}")
            # filter now assigned reports
            linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbs['reports']], linkList))
        elif "2.3." in wbs['code']:
            wbs['project']="common_mechanics"
            # assign reports
            wbs['reports']= [ rt for rt in linkList if "-CM-" in rt['name'] and any(snip in rt['name'] for snip in repList) ] 
            print(f" - wbs size: {len(wbs['reports'])}")
            # filter now assigned reports
            linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbs['reports']], linkList))
        elif "2.4." in wbs['code']:
            wbs['project']="common_electronics"
            # assign reports
            wbs['reports']= [ rt for rt in linkList if "-CE-" in rt['name'] and any(snip in rt['name'] for snip in repList) ] 
            print(f" - wbs size: {len(wbs['reports'])}")
            # filter now assigned reports
            linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbs['reports']], linkList))
        elif "2.5." in wbs['code']:
            wbs['project']="common items"
            # linkList= list( set(linkList) - set(wbs['reports']) )
        else:
            print(f"unknown code: {wbs['code']}")
        print(f" - linkList size: {len(linkList)}")

    ### catch unassigned things
    wbsList.append({'project':"pixels", 'code':"2.1.X", 'name':"unassigned", 'nickName':"NA", 'reports':[rt for rt in linkList if any(pc in rt['name'] for pc in ["P_","P-","P."])] } )
    linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbsList[-1]['reports']], linkList))
    wbsList.append( {'project':"strips", 'code':"2.2.X", 'name':"unassigned", 'nickName':"NA", 'reports':[rt for rt in linkList if any(pc in rt['name'] for pc in ["S_","S-","S."])] } )
    linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbsList[-1]['reports']], linkList))
    wbsList.append( {'project':"common_mechanics", 'code':"2.3.X", 'name':"unassigned", 'nickName':"NA", 'reports':[rt for rt in linkList if any(pc in rt['name'] for pc in ["CM_","CM-","CM."])] } )
    linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbsList[-1]['reports']], linkList))
    wbsList.append( {'project':"common_electronics", 'code':"2.4.X", 'name':"unassigned", 'nickName':"NA", 'reports':[rt for rt in linkList if any(pc in rt['name'] for pc in ["CE_","CE-","CE."])] } )
    linkList = list(filter(lambda rt: rt['name'] not in [w['name'] for w in wbsList[-1]['reports']], linkList))

    ### non-assigned reports
    wbsList.append( {'project':"other", 'code':"X.Y.Z", 'name':"miscellaneous", 'nickName':"NA", 'reports':linkList} )
    
    print("Report count:")
    totalAssigned=0
    for wbs in wbsList:
        print(f"- {wbs['code']} {wbs['nickName']}: {len(wbs['reports'])}") 
        totalAssigned+=len(wbs['reports'])
    print(f"Total assigned things: {totalAssigned}")
    if len(linkList)>1:
        print(f"Unassigned things: {len(linkList)}")
        # print(linkList)
    else:
        print(" - all things assigned")

    ###################
    ### 3. write structure: docs directories
    ###################
    print("\n### Write directories")
    docsDir="docs/"

    ### write index (with timestamp)
    print("Writing index")
    with open(docsDir+"index.md", "a") as content_file:
        ### wbs summary table
        content_file.write(IntroContent())    ### write index (with timestamp)
        ### quick summary of found reports
        content_file.write(f"__Total reports compiled: {len(linkList)+totalAssigned}__\n\n") 
        content_file.write(f"Total reports __assigned__: {totalAssigned}\n\n") 
        for pc in ["common_electronics","common_mechanics","pixels","strips"]:
            print(f" - {pc}")
            pcList=[wbs['reports'] for wbs in wbsList if pc in wbs['project']]
            content_file.write(f"- {pc.title().replace('_',' ')}: {len(pcList)} \n")
        content_file.write(f"\n## WBS Structure \n")
        content_file.write("\ncf [__ITk Organigram__](https://atlaspo.cern.ch/internal/ATLASOrganisation/index.html?value=ITK)\n\n")
        content_file.write("\n".join(BuildTableWBS([wbs for wbs in wbsList if "NA" not in wbs['nickName']]))+"\n\n")
        ### quick summary of found reports
        content_file.write(f"\n\nend document\n\n\n") 


    ### loop over wbsList
    for wbs in wbsList:
        # print(f"\n###########\n### {wbs['project']}\n###########")
        # if len(wbs['reports'])<1:
        #     continue
        ### make directory if necessary
        if wbs['project'] not in [f.path.split('/')[-1] for f in os.scandir(docsDir) if f.is_dir() ]:
            print(f"no {wbs['project']} found")
            print(f"- making: {wbs['project']}")
            os.mkdir(docsDir+wbs['project']) 
        edName=wbs['code']+"_"+wbs['nickName'].replace(' ','_')
        ### write page
        # wbs['fileName']=docsDir+wbs['project']+"/"+edName+"/"+wbs['name']+" WBS.md"
        wbs['fileName']=docsDir+wbs['project']+"/"+edName+".md"
        # else if in list...
        WriteWBSPage(wbs)


    ###################
    ### 4. use structure: update yml
    ###################
    print("### Update yml")
    with open("mkdocs.yml", "a") as mkdocs_file:
        # loop over directories
        for contDir in sorted([f.path for f in os.scandir(docsDir) if f.is_dir()]):
            dirDone=False
            if "images" in contDir:
                continue
            print(f"got dir: {contDir}")
            # write files
            for fileName in sorted([f.path for f in os.scandir(contDir) if f.is_file()]):
                print(f" - got file: {fileName}")
                label=fileName.split('/')[-1].replace('.md','')
                if label[0] in [' ','_']:
                    label=label[1::]
                if label[-1] in [' ','_']:
                    label=label[0:-1]
                if not dirDone:
                    mkdocs_file.write(f"    - {contDir.split('/')[-1]}: \n")
                    dirDone=True
                newStr=f"      - {label}: {fileName.replace(docsDir,'')}\n"
                mkdocs_file.write(newStr)
            for subDir in sorted([f.path for f in os.scandir(contDir) if f.is_dir()]):
                subDone=False
                print(f" - got sub-directory: {subDir}")
                for fileName in sorted([f.path for f in os.scandir(subDir) if f.is_file()]):
                    print(f"  - got file: {fileName}")
                    label=fileName.split('/')[-1].replace('.md','')
                    if label[0] in [' ','_']:
                        label=label[1::]
                    if label[-1] in [' ','_']:
                        label=label[0:-1]
                    if not dirDone:
                        mkdocs_file.write(f"    - {contDir.split('/')[-1]}: \n")
                        dirDone=True
                    if not subDone:
                        mkdocs_file.write(f"      - {subDir.split('/')[-1]}: \n")
                        subDone=True
                    newStr=f"      - {label}: {fileName.replace(docsDir,'')}\n"
                    mkdocs_file.write(newStr)


### for commandline running
if __name__ == "__main__":
    main()
    print("All done.")
