# Reports

Automatically generated reports: 

- Per ITk Project: reports for ITk Pixels, Strips, Common Mechanics, Common Electronics
- Others: non-project specific

## Common Dashboards

Preset dashboards are compiled in three flavours:

- ComponentType Dashboard (CTDB)
- TestType Dashboard (TTDB)
- Institution Inventory Dashboard (IIDB)
- Part Populations Dashboard (PPDB)

__Population limits__

_Currently for automated report generation is limited to report populations over the project (10,1500)_

- \>10 → try to avoid test/out-dated componentTypes
- <1500 → limits fileSize


### ComponentType Dashboard (CTDB)

Get _component_ information per _component type_ from PDB

- Primarily for data integrity checks

Contents:

- A tab per parameter - component property:

  - For each componentType (sub-)type:

    - Upload timelines

      - date: date of measurement
      - institution: institution where measurement was carried out

    - if parameter is numeric (e.g. float/int)

      - distribution of values

  - Table for all components


### TestType Dashboard (TTDB)

Get _test run_ information per _test type_ from PDB

- Primarily for data integrity checks

Contents:

- A tab per parameter - testRun property and result:

  - For each componentType (sub-)type:

    - Upload timelines

      - date: date of measurement
      - institution: institution where measurement was carried out

    - if parameter is numeric (e.g. float/int)

      - distribution of values

  - Table for all testRuns


### Institution Inventory Dashboard (IIDB)

Get _component_ information for each _component type_ per _institution_ from PDB

Content:

- Overview information tab: 

  - componentType type and current stage

- Tab per componentType: 

  - For each componentType (sub-)type:

    - _current_ stage
    
  - Stage summary: stages _visited_


### Part Populations (PPDB)

Get _component_ information for each _component type_ from PDB
 
Contents:

- Overview information tab: 

  - current location & stage

- A tab per componentType (sub-)type: 

  - current location & stage

- A tab per componentType (sub-)type: 

  - stages summary: stages visited

<!-- - stage timelines: update date -->
<!-- - Test information (per type):
    - test summary: testType populations
    - testRun timelines: measurement date  -->
