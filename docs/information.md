# Reporting

The ITk Production Database (PDB) holds all the ITk production information. Data can be extracted via scripts using the [itkdb](https://pypi.org/project/itkdb/) python package or directly via API requests (see [API documentation](https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=home)).

ITk reporting tools exist to help retrieve sub-sets of PDB information for specific tasks using pre-defined or tunable scripts. Here, several tools are documented. If there is no reporting tool to fit your reporting task then please contact: [Kenny](mailto:wraight@cern.ch)

Tools have three types:

 * [On Demand Reports](#on-demand-reports): data is compiled at the user's request
 * [Scheduled Dashboards](#scheduled-dashboards): cronned scripts to produce sharable dashboards
 * [Alerts](#alerts): reporting with compilation of "out-of-spec" values
 <!-- * [Timelines](#population-timeline-plots) for **on-demand trending** -->


### General Comments

Reports are written in Python and make use of:

 * [itkdb](https://pypi.org/project/itkdb/) package to query PDB
 * [pandas](https://pandas.pydata.org) package for dataframes to wrangle data
 * [altair](https://altair-viz.github.io/index.html) package for visualisations

addtionally for dashboard creation: 

 * [Datapane](https://datapane.com) package to compile and distribute reports

??? note "Install packages"

    To install a python package locally:
    > python -m pip install PACKAGE_NAME

--- 

## On Demand Reports

On demand reports request and compile data from PDB at the user's request.

Scripts are pre-prepared in notebook and webApp format. These can be run as they are or used as template for further development.

### Reporting Scripts

The [ITk-reports](https://gitlab.cern.ch/wraight/itk-reports) repository contains several example notebooks for reporting:

- Component Type Dashboards: what, where, stage, meta-data
- Test Type Dashboards: where, when, data
- Institution Inventory Dashboards: what, stage, meta-data

Documentation and tutorial examples is available [here](https://itk-reports.docs.cern.ch)

### Reporting WebApps

A dedicated reporting webApp contians a couple of themes for general reporting [here](https://itk-pdb-webapps-reporting.web.cern.ch)

Two themes:

__Population App__:

Retrieved information:

- PDB Stats: overall stats for database
- Project Pops: populations per componentType/testType
- Inventories: populations per Institution
- Plot Param: plot component/test parameter
- Plot Shipments: visualise component travels

Repository: [gitLab](https://gitlab.cern.ch/wraight/itk-web-apps/-/tree/master/reportingApps/populationApp)

__Reporting App__

“an overview of components at institute and country level with options to select a smaller number of countries or a single institute”

- Also deployed as part of [strips](https://itk-pdb-webapps-strips.web.cern.ch) and [pixels](https://itk-pdb-webapps-pixels.web.cern.ch) webApps
- QT credit: [Conor McPartland](mailto:conor.mcpartland@cern.ch)

Retrieved information:

- Summary Tables
- Summary Plots
- Detailed Tables
- Detailed Plots

Originally intended for Pixels monitoring components at clusters/instutions on-demand.

 - [Original documentation](https://itk.docs.cern.ch/pixels/reporting/reporting_tutorial/).

Repository: [gitLab](https://gitlab.cern.ch/wraight/itk-web-apps/-/tree/master/reportingApps/conorApp)


### WebApp Interface

WebApp to run [ITk-reports](https://gitlab.cern.ch/wraight/itk-reports) style dashboards on demand.

- QT credit: [Nihad Hidic](mailto:nihad.hidic@cern.ch)

Intended to ease interface with the general reporting codebase for common report types, e.g. testType dashboard to check upload test data, componentType dashboard to check component sub-type, stages, etc.

 - Deployment: [webApp](https://nihad-itk-qt-itk-reporting.app.cern.ch) 
 - Repository: [gitLab](https://gitlab.cern.ch/nihad/itk-qt-nihad)


---

## Scheduled Dashboards

Scheduled are made using scripts from [ITk-reports](https://gitlab.cern.ch/wraight/itk-reports) repository and run from Cern's [OKD](https://paas.docs.cern.ch) platform:

 - Data Integrity:

    - Component Type Dashboards (CTDB): data from component object parameters
    - Test Type Dashboards (TTDB): data from testRun object parameters

 - Overview:

    - Institution Inventory Dashboards (IIDB): components, stage, meta-data
    - Part Populations Dashboards (PPDB): where, stage

A repository of scheduled reports is [here](https://itk-reporting-repository.docs.cern.ch/report_notes/
)

- CTDB, TTDB, IIDB and PPDB reports are created for populations of database objects where the population is (10,2500) for each project 


---

## Alerts

Alerts are based in Test Type Dashboards which compile test data for each parameter over sets of uploaded tests.

Additional functionality:

- identify _exceptional_ data points 
- collate _exceptional_ values and associated data
- email responsible person(s)

**identify _exceptional_ data points**

Values can be identified as exceptional as follows:

- Multiples of sigma, i.e. make use of statistical information
- Range max & min, e.g. from specification documents
- Exact matching, i.e. alert if equal
- Non-matching, i.e. alert if not equal

**collate _exceptional_ values and associated meta-data**

An __Alerts__ tab is added to testType Dashboard report with a table of exceptional values for each parameter (__NB__ components may appear in multiple tables).

The tables include: tested component serialNumber, upload institution of test, stage of test, parameter code & value, reason identified as exceptional

**email responsible person(s)**

The recipient receives an email with the report summary (number of exeptions per parameter) and a link to full report dashboard.

### Set-up an alert

Please contact [Kenny](mailto:wraight@cern.ch) with:

- PDB testType
- Exception definitions (per parameter)
- Contact email

---

## Unmaintained tools

__Production Population Timelines webApp__

WebApp to monitor trends in component production over clusteers/institutions.

 - QT credit: [Zef Rozario](mailto:z.rozario.1@research.gla.ac.uk)
 - Deployment: [webApp](https://itktimelines.app.cern.ch)
 - Repository: [gitLab](https://gitlab.cern.ch/zrozario/ITk_component_population_Timeline_reporting)

---

## Credits

Extra bits

### Code repository

Code to generate these pages lives [here](https://gitlab.cern.ch/wraight/itk-reports-repository)

### Report Generation

Some documentation on generating reports with _ITk-reports_ can be found [here]().

### Additional

Built using [squidfunk](https://squidfunk.github.io/mkdocs-material/)

 - additional inspitation from [itk-docs](https://itk.docs.cern.ch)
 