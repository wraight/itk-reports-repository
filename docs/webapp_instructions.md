# Set up report

[WebApp link](https://nihad-itk-qt-itk-reporting.app.cern.ch) 

* The user can check the checkbox to see all the report scripts available
* Select report type gives the user the option to select a report script depending on what type of report the user need

![image info](images/scr1.png)

* Currently we have reports for testType_dashboard, componentType_dashboard and institute_Inventory
* By checking the Upload your spec file checkbox the user will be prompted to upload a spec file instead of creating a new one
* If the user wants to create their own spec file from scratch just leave the above checkbox unchecked and write the name of the json file in the empty field
* The user can check templates below the Create json file box
* The corresponding template will be automatically selected when choosing a report

![image info](images/scr2.png)

# User credentials

* The user can check the Enter credentials via file checkbox to upoad a json file with their credentials
* If the user checked the box there will be an example how the file should look like

![image info](images/scr5.png)

* Or the user can just fill the two fields in manually
* After that just click on the Set credentials

![image info](images/scr4.png)

# User input

The spec files is made up of four parts:
* population
* extraction
* visualisation
* distribution

Depending on the report script the user is using the spec file will have different fields

Use reset json button at the beginning, to have an empty json file

# General structure of population

* alias - ID for that population
* spec - specifications of the population
  * projCode - code of the project, e.g. S - strips
  * compTypeCode - code of the component type, e.g. MODULE
  * instCode - code of the institute, e.g. CUNI - Charles University
  * filters - for filtering through child, parent, grandChild, grandParent and keyValue

* The user can select the project code from the selectbox

![image info](images/scr6.png)

![image info](images/scr7.png)

* the user can just ignore filters or enable them by checking the checkbox

![image info](images/scr8.png)

* When the user fills out all the fields click on the Add population to add everything to their json file
* The user can add additional filters using Add aditional filters (just change the fields for filters and click the Add aditional filters button)

# General structure of extraction

* population used - the user can select what population they want to use for the extraction, by selecting the corresponding population alias
* alias - ID for that exraction
* component summary, stage order, stage summary, test summary are options that can be added to the spec file
* The user can add them by checking the checkbox, by default they will be false, the user can turn them on by checking the checkbox below them
* spec - by default it is just a string, it can be changed by checking Add multiple specs, in that case it becomes a list of dictionaries
* For specs the user can select testCode, paraCode and subs, however many the user want
* When the user fills out all the fields click on Add extraction to add everything to their json file
* The user can add additional specs using Add aditional specs (just change the fields for specs and click the Add aditional specs button)

![image info](images/scr9.png)

# General structure of visualisation

* extraction used - select extraction to be used for visualisation, can be multiple
* alias - ID for that visualisation
* component summary, stage order, stage summary, test summary, data quality are options that can be added to the spec file
* The user can add them by checking the checkbox, by default they will be false, the user can turn them on by checking the checkbox below them
* match parameter code - used in parameter comparison and parameter consistency (work in progress)
* combinations - used in parameter consistency (work in progress)
* correlations - used in parameter comparison (work in progress)
* When the user fills out all the fields click on Add visualisation to add everything to their json file
* The user can add additional combinations/correlations using Add aditional combinations/correlations buttons respectively (just change the fields for combinations/correlations and click the button)

![image info](images/scr10.png)

# General structure of distribution

* alias - ID for that distribution
* report name - the name of the report that will be shown in Datapane
* location - where the report will be saved
  * local - the report will be saved locally on the server and the user can download it below
  * remote - save the report directly to Datapane
* In case of chosing the remote option a field that asks for their datapane key will appear
* visualisation used - select visualisation to be used for distribution, can be multiple
* After the user fills out all the fields click on the Add distribution button to add everything to their json file

![image info](images/scr11.png)

# Run script

In the field write report name. After filling it out click on the Submit button.

![image info](images/scr12.png)

# Report outputs

You can see the submitted outputs.

# Report htmls

You can select a report that was saved locally and download it as a html file by clicking the Download html button.

![image info](images/scr13.png)
