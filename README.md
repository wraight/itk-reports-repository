# itk-reports-repository

Python based repository for storing and linking ITk-reports style reports.
 - reports built using [ITk-reports](https://gitlab.cern.ch/wraight/itk-reports)

> all watched over by machines of loving grace -- [Richard Brautigan](http://www.brautigan.net/machines.html)  

See published [documents](https://itk-reporting-repository.docs.cern.ch)

Required packages: 
 - mkdocs
 - mkdocs-material

### Contents

1. Script to make content pages [jump](#script-to-collect-reports)
    - make index page
    - collect reports per directory and produce summary page with links 
    - update mkdocs yaml
2. Running with docker [jump](#running-with-docker)
    - bash script for runtime includes git pull from current reporsitory
3. Adding and updating reports [jump](#adding-and-updating-reports)
    - how to send reports (datapane htmls) to repository


## Script to Collect Reports

Scripts in _scripts_ directory.

**make_content**

 - make index (front) page
    - include timestamp to check date of creation
 - collect reports per directory and produce summary page with links
    - any _html_ files in _*reports_ directories
 - update mkdocs yaml
    - include created files


## Running with Docker

Running with Docker is recommended.

Example commands:

 - build image
 > docker build -f dockerFiles/Dockerfile -t rep-app .
 - run container
 > docker run -p 8000:8000 rep-app

<!-- > docker run -it --rm -v $PWD:$PWD -w $PWD  -p 8000:8000 python:3.7-buster bash -c 'python -m pip install -U pip setuptools; \ python -m pip install -r requirements.txt; \ mkdocs serve --dev-addr 0.0.0.0:8000' -->

docker run arguments ([full documentation](https://docs.docker.com/engine/reference/commandline/run/)):
 - it: interactive
 - v: mounting volumes (__NB__ use "PWD" for Windows)
 - w: set working directory
 - p: map ports


## Adding and updating reports

The [python-gitlab](https://pypi.org/project/python-gitlab/) package can be used to script access to gitlab repositories.

Example function (from [link](https://stackoverflow.com/questions/56921192/how-to-place-a-created-file-in-an-existed-gitlab-repository-through-python)):

```python
### based on https://stackoverflow.com/questions/56921192/how-to-place-a-created-file-in-an-existed-gitlab-repository-through-python
### - but use project.commits instead of project.files
### - for commit actions (create, update, move, delete, chmod) see: https://docs.gitlab.com/ee/api/commits.html
def ScriptCommit(gitlab_url, project_id, repo_path, private_token, branch, infile_path):

    gl = gitlab.Gitlab(gitlab_url, private_token=private_token)

    try:
        # get the project by the project id
        project = gl.projects.get(project_id)
    except gitlab.exceptions.GitlabGetError as get_error:
        # project does not exists
        print(f"could not find no project with id {project_id}: {get_error}")
        return False
            
    file_data = {
                'branch': 'master',
                'commit_message': "Script commit: "+datetime.datetime.now().strftime("%H:%M, %d %B %Y"),
                'actions': [
                    {
                        'action': 'create',
                        'file_path': repo_path,
#                         'content': "the new content: "+datetime.date.today().strftime("%H:%M, %d %B %Y")
                        'content': open(infile_path,'r').read()
                    }
                ]
            }
    
    try:
        print(f"Committing: {file_data['commit_message']}")
        resp = project.commits.create(file_data)
    except gitlab.exceptions.GitlabCreateError as create_error:
        print(f"Cannot create: {create_error} (may already exist).\nTry update instead...")
        file_data['actions'][0]['action']="update"
        try:
            resp = project.commits.create(file_data)
        except gitlab.exceptions.GitlabCreateError as update_error:
            print(f"Cannot update: {update_error}. Exiting ")
            return False

    return True
```
